# TODO

- add list of pages with CCSS generated (grouped by post type: page, post, custom_post_type, etc)
- add button to generate critical css for all pages or to select one or more post types to generate for. (use https://github.com/diy/intercom.js/ to open new tabs and send a message back when ccss is generated).
- add option to hook into autoptimizes cache to clear the critical css cache (possibly this action autoptimize_action_cachepurged)
- add an option to delete all CCSS when uninstalling the plugin
- add barbajs support
- check out autoptimizes admin bar dropdown code to create something similar
- add option to only ajax css from admins
