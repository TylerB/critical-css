(function($) {
  "use strict";

  $(document).ready(function() {
    o10n.extract("critical", function(css) {
      $.ajax({
        type: "post",
        dataType: "json",
        url: ajax_object.ajaxurl,
        data: {
          action: "ajax_critical_css_to_server",
          security: ajax_object.ajax_nonce,
          css: css,
          filename: ajax_object.filename
        },
        success: function(response) {
          if (ajax_object.logged_in) {
            if (response.success == true) {
              console.log("Critical CSS generated.");
              location.reload();
            } else {
              console.log("Something went wrong..");
            }
          }
        }
      });
    });
  });
})(jQuery);
