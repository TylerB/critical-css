<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://tylerbarnes.ca
 * @since      1.0.0
 *
 * @package    Critical_Css
 * @subpackage Critical_Css/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Critical_Css
 * @subpackage Critical_Css/public
 * @author     Tyler Barnes <tyler@known.design>
 */

class Critical_Css_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Initialize by setting up the filename and path and checking if critical * CSS already exists
	 *
	 * @since    1.0.0
	 */
	function critical_css_initialize() {
		$page_id = get_the_ID();
		$post_type = get_post_type();
		$wp_content_dir = WP_CONTENT_DIR;
		global $criticalCSSdir;
		$criticalCSSdir = "$wp_content_dir/cache/criticalCSS";
		global $fileName;
		$fileName = "$post_type-$page_id";
		global $criticalCSSfile;
		$criticalCSSfile = "$criticalCSSdir/$fileName.css";

		if (file_exists($criticalCSSfile)) {
			global $add_inline_styles;
			$add_inline_styles = true;
			return;
		} else {
			$add_inline_styles = false;
			show_admin_bar(false);
		}
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		global $add_inline_styles;
		if (!$add_inline_styles) {
			wp_enqueue_script( 'critical-css-widget', plugin_dir_url( __FILE__ ) . 'js/vendor/critical-css-widget.js', array( 'jquery' ), $this->version, false );
			
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/critical-css-public.js', array( 'jquery', 'critical-css-widget' ), '1.0.0', false );

			$page_id = get_the_ID();
			$post_type = get_post_type();
			global $fileName;
			$fileName = "$post_type-$page_id";

			$params = array(
			'ajaxurl' => admin_url('admin-ajax.php'),
			'ajax_nonce' => wp_create_nonce('criticalCSS Nonce'),
			'filename' => $fileName,
			'logged_in' => is_user_logged_in()
			);
			wp_localize_script( $this->plugin_name, 'ajax_object', $params );
		}
	}

	/**
	 * Remove css comments from an input string
	 *
	 * @since    1.0.0
	 */
	protected function remove_css_comments($css) {
		$removeCommentsPattern = '!/\*[^*]*\*+([^/][^*]*\*+)*/!'; 
		return preg_replace(
							$removeCommentsPattern, 
							'', 
							$css
						);
	}

	/**
	 * Echo critical CSS inline to the <head></head>
	 *
	 * @since    1.0.0
	 */
	public function critical_css_add_inline_styles() {
		global $add_inline_styles;
		if (!$add_inline_styles) {
			return false;
		}

		global $criticalCSSfile;
		
		$inputCssNoComments = $this->remove_css_comments(
			file_get_contents($criticalCSSfile)
		);

		$critical_css = "
		<style>$inputCssNoComments</style>";

		echo $critical_css;
	}

	/**
	 * Sanitize client ajaxed critical css
	 *
	 * @since    1.0.0
	 */
	protected function clean_critical_css($input_css) {
		$sanitizedCss = sanitize_textarea_field($input_css);
		return $sanitizedCss;
	}

	/**
	 * Write the ajaxed critical CSS to the wp-content/criticalCSS dir
	 *
	 * @since    1.0.0
	 */
	protected function write_critical_css_to_file(
		$inputCss, 
		$criticalCSSfile, 
		$criticalCSSdir
		) {

			if (!file_exists($criticalCSSdir)) {
				mkdir($criticalCSSdir, 0755, true);
			}

			require_once plugin_dir_path( __FILE__ ) . 'vendor/php-js-css-minify.php';
			$minifiedCss = minify_css($inputCss);

			$myfile = fopen($criticalCSSfile, "w") or die("Unable to open file!");
			fwrite($myfile, $minifiedCss);
			fclose($myfile);
			return true;
	}

	/**
	 * Receive ajaxed critical CSS and process it.
	 *
	 * @since    1.0.0
	 */
	public function ajax_critical_css_to_server() {
		check_ajax_referer( 'criticalCSS Nonce', 'security' );

		$inputCss = $_POST['css'];
		$fileName = $_POST['filename'];
		$wp_content_dir = WP_CONTENT_DIR;
		$criticalCSSdir = "$wp_content_dir/cache/criticalCSS";
		$criticalCSSfile = "$criticalCSSdir/$fileName.css";

		if (!file_exists($criticalCSSfile)) {
			$cleaned_css = $this->clean_critical_css($inputCss);

			$this->write_critical_css_to_file(
				$cleaned_css, 
				$criticalCSSfile, 
				$criticalCSSdir
			);

			wp_send_json_success();
		}
	
		die();
	}

}
