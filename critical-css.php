<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://tylerbarnes.ca
 * @since             1.0.0
 * @package           Critical_Css
 *
 * @wordpress-plugin
 * Plugin Name:       Critical CSS
 * Plugin URI:        https://tylerbarnes.ca
 * Description:       Automated critical CSS generated on first page load
 * Version:           1.0.0
 * Author:            Tyler Barnes
 * Author URI:        https://tylerbarnes.ca
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       critical-css
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once plugin_dir_path( __FILE__ ) . 'includes/class-critical-css-debug.php';

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-critical-css-activator.php
 */
function activate_critical_css() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-critical-css-activator.php';
	Critical_Css_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-critical-css-deactivator.php
 */
function deactivate_critical_css() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-critical-css-deactivator.php';
	Critical_Css_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_critical_css' );
register_deactivation_hook( __FILE__, 'deactivate_critical_css' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-critical-css.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_critical_css() {

	$plugin = new Critical_Css();
	$plugin->run();

}
run_critical_css();
