<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://tylerbarnes.ca
 * @since      1.0.0
 *
 * @package    Critical_Css
 * @subpackage Critical_Css/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Critical_Css
 * @subpackage Critical_Css/includes
 * @author     Tyler Barnes <tyler@known.design>
 */
class Critical_Css_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
