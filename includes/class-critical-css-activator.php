<?php

/**
 * Fired during plugin activation
 *
 * @link       https://tylerbarnes.ca
 * @since      1.0.0
 *
 * @package    Critical_Css
 * @subpackage Critical_Css/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Critical_Css
 * @subpackage Critical_Css/includes
 * @author     Tyler Barnes <tyler@known.design>
 */
class Critical_Css_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
