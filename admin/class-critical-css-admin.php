<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://tylerbarnes.ca
 * @since      1.0.0
 *
 * @package    Critical_Css
 * @subpackage Critical_Css/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Critical_Css
 * @subpackage Critical_Css/admin
 * @author     Tyler Barnes <tyler@known.design>
 */

 require_once plugin_dir_path( __FILE__ ) . "/vendor/admin-page-framework/library/apf/admin-page-framework.php";

 class Critical_Css_CreatePage extends AdminPageFramework {
    public function setUp() {
        // Create the root menu - specifies to which parent menu to add.
        $this->setRootMenuPage( 'Settings' );  
 
        // Add the sub menus and the pages.
        $this->addSubMenuItems(
            array(
                'title'     => 'Critical CSS',  // page and menu title
                'page_slug' => 'critical_css'     // page slug
            )
        );
 
	}
	
	public function do_critical_css() {   
 
			$wp_content_dir = WP_CONTENT_DIR;
			$criticalCSSdir = "$wp_content_dir/cache/criticalCSS";
			$io = popen ( '/usr/bin/du -sk ' . $criticalCSSdir, 'r' );
			$size = fgets ( $io, 4096);
			$size = substr ( $size, 0, strpos ( $size, "\t" ) );
			pclose ( $io );
		?>

		<?php if ($size): ?>
		<h3>Cache size: <?php echo $size; ?> kb</h3>
		<div js-clear-critical-css class="button button-primary">
			Clear Cache
		</div>
		<?php else: ?>
		<h3>Cache Size: 0 kb</h3>
		<p>Visit some pages to generate criticalCSS.</p>
		<?php endif; ?>
        <?php
 
	}

	public function doCriticalCss( $oFactory ) {
		// do something here.
		// write_log('test');
	}
}

new Critical_Css_CreatePage;


class Critical_Css_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Critical_Css_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Critical_Css_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/critical-css-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Critical_Css_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Critical_Css_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/critical-css-admin.js', array( 'jquery' ), $this->version, false );

		$params = array(
			'ajaxurl' => admin_url('admin-ajax.php'),
			'ajax_nonce' => wp_create_nonce('criticalCSS Nonce'),
		);
		wp_localize_script( $this->plugin_name, 'ajax_object', $params );

	}

	/**
	 * Clear the critical css cache
	 *
	 * @since    1.0.0
	 */
	public function clear_critical_css() {
		$wp_content_dir = WP_CONTENT_DIR;
		$criticalCSSdir = "$wp_content_dir/cache/criticalCSS";

		if (file_exists($criticalCSSdir)) {
			
			$it = new RecursiveDirectoryIterator($criticalCSSdir, RecursiveDirectoryIterator::SKIP_DOTS);
			$files = new RecursiveIteratorIterator($it,
						RecursiveIteratorIterator::CHILD_FIRST);
			foreach($files as $file) {
				if ($file->isDir()){
					rmdir($file->getRealPath());
				} else {
					unlink($file->getRealPath());
				}
			}
			rmdir($criticalCSSdir);	
			return true;
		} 

		return false;
	}

	public function clear_critical_css_ajax() {
		check_ajax_referer( 'criticalCSS Nonce', 'security' );
		
		$clear_critical_css = $this->clear_critical_css();
		
		if ($clear_critical_css) {
			wp_send_json_success();
		} else {
			wp_send_json_error();
		}

		die();
	}

}
