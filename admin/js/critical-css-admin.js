(function($) {
  "use strict";

  $(document).ready(function() {
    $("[js-clear-critical-css]").on("click", function() {
      $.ajax({
        type: "post",
        dataType: "json",
        url: ajax_object.ajaxurl,
        data: {
          action: "clear_critical_css_ajax",
          security: ajax_object.ajax_nonce
        },
        success: function(response) {
          if (response.success == true) {
            console.log("critical css cleared!");
            location.reload();
          } else {
            console.log("Something went wrong..");
          }
        }
      });
    });
  });
})(jQuery);
